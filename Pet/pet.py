from threading import Thread
import time

WAIT_TIME = 5

class VirtualPet(object):

    def __init__(self):
        self.hunger = 50
        self.tiredness = 50
        self.happiness = 50
        self.fullness = 50

    def getStats(self):
        stats = {
            "Hunger": self.hunger,
            "Tiredness": self.tiredness,
            "Happiness": self.happiness,
            "Fullness": self.fullness
        }
        return stats
    def feed(self):
        self.hunger -= 10
        self.fullness += 10
        
    def play(self):
        self.happiness += 10
        self.tiredness += 10

    def sleep(self):
        self.tiredness -=10

    def poop(self):
        self.fullness -= 10

    def isDead(self):
        return self.hunger >= 100

    def onTimePassed(self):
        self.tiredness += 10
        self.hunger += 10
        self.happiness -= 10


class Game(object):
    def __init__(self):
        self.Pet = None
        self.QUIT = False
        self.backgroundThread = Thread(target=self.backgroundClock)
        self.userInputThread = Thread(target=self.takeInput)

    def start(self):
        self.Pet = VirtualPet()
        self.backgroundThread.start()
        self.userInputThread.start()
    def showStats(self):
        stats = self.Pet.getStats()
        for statName, statValue in stats.items():
            print("{0} : {1}".format(statName, statValue))

    def backgroundClock(self):
        while not self.Pet.isDead() and not self.QUIT:
            self.showStats()
            time.sleep(WAIT_TIME)
            self.Pet.onTimePassed()
    
    def takeInput(self):
         while True and not self.QUIT:
            userInput = input(":: ")
            if userInput == "q":
                self.QUIT = True
                break




def main():
    # Pet = VirtualPet()
    # t = Thread(target=timePasses, args=(Pet,))
    # t.start()
    myGame = Game()
    myGame.start()


if __name__ == "__main__":
    main()