import unittest
from pet import VirtualPet

class TestPetProgram(unittest.TestCase):

    def setUp(self):
        self.Pet = VirtualPet()

    def test_get_stats(self):
        stats = self.Pet.getStats()
        self.assertEqual(stats["Hunger"], 50)
        self.assertEqual(stats["Tiredness"], 50)
        self.assertEqual(stats["Happiness"], 50)
        self.assertEqual(stats["Fullness"], 50)
        
    def test_feeding(self):
        self.Pet.feed()
        self.assertEqual(self.Pet.hunger, 40)
        self.assertEqual(self.Pet.fullness, 60)

    def test_playing(self):
        self.Pet.play()
        self.assertEqual(self.Pet.happiness, 60)
        self.assertEqual(self.Pet.tiredness, 60)

    def test_sleep(self):
        prevTiredness = self.Pet.tiredness
        self.Pet.sleep()
        self.assertLess(self.Pet.tiredness, prevTiredness)

    def test_poop(self):
        prevFullness = self.Pet.fullness
        self.Pet.poop()
        self.assertLess(self.Pet.fullness, prevFullness)

    def test_time_passing(self):
        self.Pet.onTimePassed()
        self.assertEqual(self.Pet.hunger, 60)
        self.assertEqual(self.Pet.tiredness, 60)
        self.assertEqual(self.Pet.happiness, 40)

    def test_is_dead(self):
        self.Pet.onTimePassed()
        self.Pet.onTimePassed()
        self.Pet.onTimePassed()
        self.Pet.onTimePassed()
        self.assertFalse(self.Pet.isDead())
        self.Pet.onTimePassed()
        self.Pet.onTimePassed()
        self.Pet.onTimePassed()
        self.Pet.onTimePassed()
        self.assertTrue(self.Pet.isDead())


if __name__ == "__main__":
    unittest.main()