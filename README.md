# Pair Programming Week

A set of challenges completed using Test-Driven-Development and Pair-Programming.

Challenges are found [here](https://gitlab.com/thgaccelerator/Core_Curriculum/blob/master/technical_content/agile/pairing/pairing_challenge.md)

### Challenges

- [Fizz Buzz](/Fizzbuzz)
    - Completed by Elena Waite and James Young
    - Monday 26th November 2018

- [Tamagotchi](/Tamagotchi)
    - Completed by Alexa Powell and James Young
    - Tuesday 27th November 2018

- [Vending Machine](/VendingMachine)
    - Completed by Simon Williams and James Young
    - Wednesday 28th November 2018

- [Bowling](/Bowling)
    - Completed by Ruiheng Li, Yin Kwan and James Young
    - Thursday 28th November 2018