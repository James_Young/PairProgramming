#!/usr/bin/env python
from threading import *
import time

x = 0
def handleClient1():
    global x
    while(True):
        
        x += 1
        time.sleep(5) # wait 5 seconds      
 
def handleClient2():
    while(True):
        y = input("Enter: ")
        print(x)
        
 
# create threads
t = Timer(0.0, handleClient1)
t2 = Timer(1.0, handleClient2)
 
# start threads
t.start()
t2.start()