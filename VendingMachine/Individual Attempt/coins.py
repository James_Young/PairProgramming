class Coin(object):
    def __init__(self, name, value):
        self.name = name
        self.value = value

class Quarter(Coin):
    def __init__(self):
        super(Quarter, self).__init__("quarter", 0.25)

class Dime(Coin):
    def __init__(self):
        super(Dime, self).__init__("dime", 0.1)

class Nickel(Coin):
    def __init__(self):
        super(Nickel, self).__init__("nickel", 0.05)

class Penny(Coin):
    def __init__(self):
        super(Penny, self).__init__("penny", 0.01)


def makeCoin(coinName):
    if coinName == "dime":
        return Dime()
    elif coinName == "nickel":
        return Nickel()
    elif coinName == "quarter":
        return Quarter()
    elif coinName == "penny":
        return Penny()
    return None
