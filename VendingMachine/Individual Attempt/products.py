class Product(object):
    def __init__(self, name, price):
        self.name = name
        self.price = price

class Cola(Product):
    def __init__(self):
        super(Cola, self).__init__("cola", 1.0)

class Crisps(Product):
    def __init__(self):
        super(Crisps, self).__init__("crisps", 0.5)

class Chocolate(Product):
    def __init__(self):
        super(Chocolate, self).__init__("chocolate", 0.65)

