import unittest
from vendingMachine import *
from products import *
from coins import *

VM = VendingMachine()

class VendingMachineTest(unittest.TestCase):

    def setUp(self):
        print("Testing Started")

    def tearDown(self):
        print("Testing Finished")

    def test_get_initial_money_total(self):
        self.assertEqual(VM.getCurrentAmount(), 0)
    
    def test_insert_coints(self):
        coins = [ Dime() ]
        VM.insertCoins(coins)
        self.assertEqual(VM.getCurrentAmount(), 0.1)
        coins = [Penny()]
        VM.insertCoins(coins)
        self.assertEqual(VM.getCurrentAmount(), 0.1)
        coins = [Nickel()]
        VM.insertCoins(coins)
        self.assertEqual(VM.getCurrentAmount(), 0.15)
    
    def test_select_product(self)
        coins = [ Quarter() ]*10
        VM.insertCoins(coins) 
        VM.selectProduct("cola")
        


if __name__ == "__main__":
    unittest.main()