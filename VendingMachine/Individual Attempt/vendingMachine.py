from products import *
from coins import *


class VendingMachine(object):

    def __init__(self):
        self.__acceptedCoins = [
            "dime",
            "quarter",
            "nickel"
        ]
        self.__storedCoins = {
            "dime" : 0,
            "nickel" : 0,
            "penny" :0
        }

        self.__productStock = [
            "cola": 10,
            "chocolate": 5,
            "crisps"
        ]

        self.__currentValue = 0

    def __moneyTotal(self):
        total = 0
        for coinName, coinCount in self.__storedCoins.items():
            coin = makeCoin(coinName)
            total += coin.value * coinCount
        return float(int(100*total))/100
    
    def __acceptCoin(self, coin):
        return coin.name in self.__acceptedCoins

    def __storeCoin(self, coin):
       self.__storedCoins[coin.name] += 1
       self.__currentValue += coin.value
    
    def getCurrentAmount(self):
            return float(int(100*self.__currentValue))/100

    def insertCoin(self, coin):
        if self.__acceptCoin(coin):
            self.__storeCoin(coin)
            return True
        return False

    def insertCoins(self, coins):
        for coin in coins:
            self.insertCoin(coin)

    def selectProduct(self, product):
        