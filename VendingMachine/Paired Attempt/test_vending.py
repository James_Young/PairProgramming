import unittest  #, vending

from vending import *


class vending_test(unittest.TestCase):
    def setUp(self):
        print('Start before the test')

    def tearDown(self):
        print('Print after the test')

    def test_get_current_amount(self):
        vending = vending_machine()
        self.assertEqual(vending.get_current_amount, 0)

    def test_insert_coins(self):
        coins = {'Nickles':1, 'Dimes':2, 'Quarters': 3, 'Pennies':4}
        vending = vending_machine()
        vending.insert_coins(coins)
        self.assertEqual(vending.get_current_amount, 1)

    def test_display_says_insert_coins(self):
        vending = vending_machine()
        self.assertEqual(vending.get_display, "INSERT COINS")

    def test_fanta_not_exist(self):
        vending = vending_machine() 
        product_list = vending.get_products()
        selection = "fanta"
        self.assertEqual(selection in product_list, False) 

    def test_cola_exist(self):
        vending = vending_machine() 
        product_list = vending.get_products()
        selection = "cola"
        self.assertEqual(selection in product_list, True) 

    def test_check_not_enough_money(self):
        vending = vending_machine()
        product = "cola"
        product_price = vending.get_product_price(product)
        current_amount = vending.get_current_amount
        self.assertEqual(current_amount >= product_price, False) 
 
    def test_check_enough_money(self):
        vending = vending_machine()
        product = "cola"
        coins = {"Quarters": 4}
        vending.insert_coins(coins)
        product_price = vending.get_product_price(product)
        current_amount = vending.get_current_amount
        self.assertEqual(current_amount >= product_price, True) 
    
    # def test_check_enough_stock(self):
    #     vending = vending_machine()
    #     product = "cola"
    #     coins = {"Quarters": 4}
    #     vending.insert_coins(coins)
    #     stock = vending.get_product_stock(product)
    #     self.assertEqual()

    def test_check_display_thank_you(self):
        vending = vending_machine()
        product = "cola"
        coins = {"Quarters": 4}
        vending.insert_coins(coins)
        vending.make_a_sale()
        machine_display = vending.get_display
        self.assertEqual(machine_display, "THANK YOU") 

    def test_update_selection(self):
        vending = vending_machine()
        product = "cola"
        coins = {"Quarters": 4}
        vending.insert_coins(coins)
        cola = ["cola", "cola"]
        select_cola = vending.select_products(cola)
        self.assertEqual(vending.get_selection["cola"], 2) 

    def test_update_stock(self):
        vending = vending_machine()
        product = "cola"
        coins = {"Quarters": 4}
        vending.insert_coins(coins)
        cola = ["cola", "cola"]
        select_cola = vending.select_products(cola)
        self.assertEqual(vending.get_selection["cola"], 2) 

if __name__ == '__main__':
    unittest.main()