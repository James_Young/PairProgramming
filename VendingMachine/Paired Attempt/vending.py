class vending_machine(object):
    def __init__(self):
        self.current_amount = 0
        self.products = {}
        self.accepted_coins = {'Nickles':0.05, 'Dimes':0.10, 'Quarters': 0.25}
        self.total = 0
        self.display = "INSERT COINS"
        self.product_names = ["cola", "crisps", "chocolate"]
        self.product_stock = {
            "cola": 5,
            "crisps": 5,
            "chocolate": 5
        }
        self.product_price = {
            "cola": 1,
            "crisps": 0.5,
            "chocolate": 0.65
        }
        self.selection = {"cola":0, "crisps":0, "chocolate":0}

    def insert_coins(self, coins):
        for coin in coins:
            if coin in self.accepted_coins:
                self.current_amount += coins[coin] * self.accepted_coins[coin]
            # else:
            #     self.display = "You can't enter {}s!".format(coin)
        # self.display = "You have entered ${}".format(self.current_amount)

    

    def select_products(self, selection):
        for product in selection:
            self.selection[product] += 1

    def update_stock(self):
        for product in self.get_selection:
            self.product_stock[product] -= self.get_selection[product]
            self.get_selection[product] = 0

    def update_current_ammount(self):
        self.current_amount -= self.get_selection_value()

    def get_selection_value(self):
        selection_value = 0
        for product, number in self.selection.items():
            selection_value += self.product_price[product] * number
        return selection_value

    def check_selection_value(self):
        return self.get_current_amount >= self.get_selection_value() 

    def check_product_in_stock(self, product):
        return self.product_stock[product] > 0

    def check_selection_in_stock(self):
        for product, number in self.selection.items():
            if number > self.product_stock[product]:
                return False
        return True

    def make_a_sale(self):
        if self.check_selection_value() and self.check_selection_in_stock():
            self.update_current_ammount()
            self.update_stock()
            self.display = "THANK YOU"

    def return_coins(self):
        self.current_amount = 0

    @property
    def get_current_amount(self):
        return self.current_amount

    @property
    def get_display(self):
        return self.display

    def show_display(self):
        print(self.display)
        self.display = "INSERT COINS"

    @property
    def get_selection(self):
        return self.selection

    def get_products(self):
        return self.product_names

    def get_product_price(self, product):
        return self.product_price[product]

def main():
    VM = vending_machine()
    print("Welcome to the Vending Machine!!")
    
    coins = {"Quarters": 10}
    product_selection = ["cola"]

    VM.show_display()
    VM.insert_coins(coins)
    VM.show_display()
    VM.select_products(product_selection)
    VM.show_display()
    VM.make_a_sale()
    VM.show_display()
    VM.show_display()
if __name__ == "__main__":
    main()